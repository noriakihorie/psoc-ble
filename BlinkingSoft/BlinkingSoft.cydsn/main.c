/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>

#define RED (0)
#define GREEN (1)
#define BLUE (2)
#define COL_MAX (3)

#define LUMI_MAX (24)

struct status {
    int stat;   // 0:RGB 1:GBR 2:BRG
    int ud;     // 0:up 1:down
    int rgb;    // 0:r 1:g 2:b
    int rgbArr[3]; //luminance
};
void change (struct status* s);

int main()
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    struct status s = {0, 0, 0, {0 ,0 ,0}};
    int i = 0;
    for(;;)
    {
        if(i % LUMI_MAX == 0) {
            change(&s);
        }
        PO_LED_R_Write((s.rgbArr[RED] > i % LUMI_MAX)? 0 : 1);
        PO_LED_G_Write((s.rgbArr[GREEN] > i % LUMI_MAX)? 0 : 1);
        PO_LED_B_Write((s.rgbArr[BLUE] > i % LUMI_MAX)? 0 : 1);
        i++;
        CyDelay(1);
    }
}
void change (struct status* s) {
    // main
    if(s->ud == 0) {
        // up luminance case
        s->rgbArr[s->rgb]++;
        if(s->rgbArr[RED] >= LUMI_MAX && 
            s->rgbArr[GREEN] >= LUMI_MAX &&
            s->rgbArr[BLUE] >= LUMI_MAX) {
            
            s->ud = 1;
            s->rgb++;
            s->rgb %= COL_MAX;
        } else {
            if(s->rgbArr[s->rgb] >= LUMI_MAX) {
                s->rgb++;
                s->rgb %= COL_MAX;
            }
        }
    } else {
        // down luminance case
        s->rgbArr[s->rgb]--;
        if(s->rgbArr[RED] <= 0 && 
            s->rgbArr[GREEN] <= 0 &&
            s->rgbArr[BLUE] <= 0) {

            s->stat++;
            s->stat %= COL_MAX;
            s->rgb = s->stat;
            s->ud = 0;
        } else {
            if(s->rgbArr[s->rgb] <= 0) {
                s->rgb++;
                s->rgb %= COL_MAX;
            }
        }
    }
}
/* [] END OF FILE */
